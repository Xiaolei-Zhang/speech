package speechsdk.quickstart;

import com.microsoft.cognitiveservices.speech.*;
import com.microsoft.cognitiveservices.speech.audio.AudioConfig;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class Speech {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        //Find your key and resource region under the 'Keys and Endpoint' tab in your Speech resource in Azure Portal
        //Remember to delete the brackets <> when pasting your key and region!
        SpeechConfig speechConfig = SpeechConfig.fromSubscription("4ede77ca04ca4dc6b80f9fe1272fc1ce", "eastasia");
        synthesizeToSpeaker(speechConfig);
    }

    public static void synthesizeToSpeaker(SpeechConfig speechConfig) throws InterruptedException, ExecutionException {
        AudioConfig audioConfig = AudioConfig.fromDefaultSpeakerOutput();
        SpeechSynthesizer synthesizer = new SpeechSynthesizer(speechConfig, audioConfig);
        SpeechSynthesisResult result= synthesizer.SpeakText("你好捷音智能");
        System.currentTimeMillis();
    }
}