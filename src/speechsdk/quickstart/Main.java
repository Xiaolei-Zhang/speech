//
// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE.md file in the project root for full license information.
//
// <code>
package speechsdk.quickstart;

import java.util.concurrent.Future;
import com.microsoft.cognitiveservices.speech.*;

/**
 * Quickstart: synthesize speech using the Speech SDK for Java.
 */
public class Main {

    /**
     * @param args Arguments are ignored in this sample.
     */
    public static void main(String[] args) {

        // Replace below with your own subscription key
        String speechSubscriptionKey = "4ede77ca04ca4dc6b80f9fe1272fc1ce";
        // Replace below with your own service region (e.g., "westus").
        String serviceRegion = "eastasia";

        // Creates an instance of a speech synthesizer using speech configuration with specified
        // subscription key and service region and default speaker as audio output.
        SpeechConfig config = SpeechConfig.fromSubscription(speechSubscriptionKey, serviceRegion);
        config.setSpeechSynthesisLanguage("zh-CN");
        config.setSpeechSynthesisOutputFormat(SpeechSynthesisOutputFormat.Audio24Khz48KBitRateMonoMp3);
        try (
             SpeechSynthesizer synth = new SpeechSynthesizer(config)) {

            assert(config != null);
            assert(synth != null);

            int exitCode = 1;

//            System.out.println("Type some text that you want to speak...");
//            System.out.print("> ");
//            String text = new Scanner(System.in).nextLine();
            String text = "你好";
            SpeechSynthesisResult result = synth.SpeakText(text);
            Future<SynthesisVoicesResult> future=synth.getVoicesAsync();
            SynthesisVoicesResult r=future.get();
            assert (r!=null);
            assert(result != null);

            if (result.getReason() == ResultReason.SynthesizingAudioCompleted) {
                System.out.println("Speech synthesized to speaker for text [" + text + "]");
                exitCode = 0;
            }
            else if (result.getReason() == ResultReason.Canceled) {
//                SpeechSynthesisCancellationDetails cancellation = SpeechSynthesisCancellationDetails.fromResult(result);
//                System.out.println("CANCELED: Reason=" + cancellation.getReason());
//
//                if (cancellation.getReason() == CancellationReason.Error) {
//                    System.out.println("CANCELED: ErrorCode=" + cancellation.getErrorCode());
//                    System.out.println("CANCELED: ErrorDetails=" + cancellation.getErrorDetails());
//                    System.out.println("CANCELED: Did you update the subscription info?");
//                }
            }

            System.exit(exitCode);
        } catch (Exception ex) {
            System.out.println("Unexpected exception: " + ex.getMessage());

            assert(false);
            System.exit(1);
        }
    }
}
// </code>
